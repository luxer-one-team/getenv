<?php

declare(strict_types=1);

namespace LuxerOne\Getenv;

function getenvAsString(
    string $key,
    bool $localOnly = false
): string {
    $value = \getenv($key, $localOnly);
    if (is_string($value)) {
        return $value;
    }

    throw new \InvalidArgumentException(sprintf(
        'Environment variable %s is not set',
        $key
    ));
}

function getenvAsStringWithDefault(
    string $key,
    string $default,
    bool $localOnly = false
): string {
    $value = \getenv($key, $localOnly);
    if (is_string($value)) {
        return $value;
    }

    return $default;
}

function getenvAsBoolDefaultToFalse(
    string $key,
    bool $localOnly = false
): bool {
    return \filter_var(
        \getenv($key, $localOnly),
        FILTER_VALIDATE_BOOLEAN,
        ['default' => false]
    );
}
